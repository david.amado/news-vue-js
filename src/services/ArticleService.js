import axios from 'axios'

var apliClient = axios.create({
  baseURL: 'http://newsapi.org/v2/',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }
})

export default {
  getTopHeadlinesArticles(page=1,country='co', q=''){//country, category, source & Q(query)
    return apliClient.get('/top-headlines/?pageSize=10&page='+page+'&country='+country+'&q='+q+'&apiKey=5dc0adcc337442a3b8d9488aeca28d9a')
  },
  getEverythingArticles(page=1,q='', from='', to=''){// Q(query) & from-to date
    return apliClient.get('/everything/?pageSize=10&page='+page+'&q='+q+'&from='+from+'&to='+to+'&apiKey=5dc0adcc337442a3b8d9488aeca28d9a')
  },
}
