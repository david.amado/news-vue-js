import Vue from "vue";
import VueRouter from "vue-router";
import ArticlesList from "@/views/ArticlesList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "ArticlesList",
    component: ArticlesList
  }
];

const router = new VueRouter({
  routes
});

export default router;
