import Vue from "vue";
import Vuex from "vuex";
import ArticleService from "@/services/ArticleService.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    topHeadlinesArticles: [],
    totalTopHeadlines: 0,
    everythingArticles: [],
    totalEverything: 0,

  },
  mutations: {
    SET_TOP_HEADLINES_NEWS(state, data){
      state.topHeadlinesArticles = data.articles
      if (data.totalResults<100) {
        state.totalTopHeadlines = data.totalResults
      }else{
        state.totalTopHeadlines = 100
      }
    },
    SET_EVERYTHING_NEWS(state, data){
      state.everythingArticles = data.articles
      if (data.totalResults<100) {
        state.totalEverything = data.totalResults
      }else{
        state.totalEverything = 100
      }

    },
  },
  actions: {
    fetchTopHeadlinesArticles({ commit },{page, country, q}){
      ArticleService.getTopHeadlinesArticles(page, country, q)
      .then(response => {
        commit('SET_TOP_HEADLINES_NEWS', response.data)
      })
      .catch(error => {
        console.log("There was an error: "+error.response)
      })
    },
    fetchEverythingArticles({ commit },{page,q,from,to}){
      ArticleService.getEverythingArticles(page,q,from, to)
      .then(response => {
        commit('SET_EVERYTHING_NEWS', response.data)
      })
      .catch(error => {
        console.log("There was an error: "+error.response)
      })
    }
  },
  modules: {}
});
